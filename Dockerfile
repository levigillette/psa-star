FROM ruby:3.0.2-buster
MAINTAINER lg@broadpeakpartners.com

RUN apt update && apt upgrade -y

# Apt installs.
RUN apt install -y curl wget ripgrep tree cargo

# Download precompiled xsv b/c it's not in apt.
RUN wget -qO xsv.tar.gz https://github.com/BurntSushi/xsv/releases/download/0.13.0/xsv-0.13.0-x86_64-unknown-linux-musl.tar.gz && tar xzf xsv.tar.gz && mv xsv /usr/bin/ && ls -l /usr/bin/xsv

# Ruby gems.
RUN gem install sequel sqlite3 edn edn_turbo

COPY . /app
WORKDIR /app
