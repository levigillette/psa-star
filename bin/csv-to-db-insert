#!/usr/bin/env ruby

require 'csv'
require 'optparse'

PARAMS = {
  upsert_key: nil,
  slice_size: 10000,
}

OptionParser.new do |opt|
  opt.on("-k", "--on-duplicate-key=STRING", String, "Unique key for UPSERT check") do |comma_sep_keys_string|
    PARAMS[:upsert_key] = comma_sep_keys_string.split(/,/)
  end
  opt.on("-s", "--slice-size=SIZE", Integer, "Batch inserts into into statements with SIZE records per statement") do |slice_size|
    PARAMS[:slice_size] = slice_size.to_i
  end
end.parse!

TABLE_NAME = ARGV.shift or raise "Missing table name in ARGV[0]"

# STDERR.puts PARAMS.to_s

sanitize_sql_string = -> s { s.gsub("'", "''") }
quote_wrap_sql_string = -> s { "'" + s.to_s + "'" }

csv = CSV.new(STDIN)
cols = csv.first

csv.each_slice(PARAMS[:slice_size]) do |slice|
  puts "INSERT INTO #{TABLE_NAME} (#{cols.join(",")}) VALUES\n"
  slice.each_with_index do |tup, i|
    print "," if i > 0
    puts "(" + tup.map(&sanitize_sql_string).map(&quote_wrap_sql_string).join(",") + ")"
  end
  if PARAMS[:upsert_key]
    on_conflict_do_update_clause = cols.map { |c| "#{c} = EXCLUDED.#{c}" }.join("\n,")
    puts "ON CONFLICT (#{PARAMS[:upsert_key].join(",")}) DO UPDATE SET"
    puts on_conflict_do_update_clause
  end
  puts ";\n\n"
end
