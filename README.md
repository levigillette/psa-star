# Open issues


## Cron-style triggering

Some ideas and options:

Option A: Cron on k3 server(s)

  - Problem: There are at least 2 K3-Server hosts running in deplyed envs, and
    setting up a crontab on one does it for both causing at best unnecessary
    compution on the part of the k3 server hosts + Postgres server host. At
    worst, race conditions could occur if jobs are not parallelizable.

**Option B: Cron on some other server**

**Option C: Clojure-based thread with time execution**

## File detection strategy

### [TODO] File detection strategy to know when to process a new, previously unprocessed, file

We want a feature that can watch a local or remote (ssh/sftp/s3) directory for new, previously unprocessed files.

It needs to maintain state somewhere - preferably in the PSA Star database (e.g., `psa_{entity}__processed_files`) - a list of records containing:

- Example record: `{:url "file:///my/csv/files/foo_123.csv", :mtime "2021-10-29T13:34:56Z", :md5sum "deadbeefdeadbeefdeadbeefdeadbeef"}`

The algorithm that will run periodically (cron-style) - will:

1. Pull list of files in the directory.
2. Filter out files that have been processed and the modification time (mtime) is the same.
3. If name if same but mtime is changed, consider it new.
4. For each 'new' file:
   1. Run a hash digest (md5sum) on the new file.
   2. If hash is same as before, update file's mtime on the record and discard file from further processing.
   3. For all 'new' files, process and record them (name, mtime, md5sum).

## Environment variable strategy
### [TODO] PostgreSQL URL

Stabilize the required environment variables. Currently the only environment variable we need is a connection to a postgres database. Example:

`PSA_STAR_POSTGRES_URI='postgresql://psastaruser@your-psa-star-db-host.us-east-1.rds.amazonaws.com/star?ssl=true&sslmode=require&password=YourSecretPassword123'`

## Configuration data
### [DONE] Allow passing config as string rather than just file name (file name still works though).

Current state behavior - config must be in local file:

```
< outage_20211028.csv bin/csv-to-psa-format config/outage_config.edn
```

Desired state behavior - get config from string (permits numerous options):

(Option A: Literal string)

```
< outage_20211028.csv | ... | bin/csv-to-psa-format '{:nk_cols [:outage_id], :copy_cols {:release_date :vtime}, :expect_cols {:outage_id :int, :vtime :db_timestamp_string, :outage_status :string, :outage_type :string, :unit_id :int, :plant_id :int, :start_date :db_date_string, :end_date :db_date_string, :release_date :db_timestamp_string, :capacity_offline :float, :capacity_uom :string, :snap_date :db_date_string}}' | ...
```

(Option B: From a file)

```
< outage_20211028.csv | ... | bin/csv-to-psa-format config/outage_config.edn | ...
```

(Option C: From database)

```bash
< outage_20211028.csv | ... | bin/csv-to-psa-format "$(psl -c "select config from psa__entity where entity = 'outage'")" | ...
```

## Docker workflows

### [DONE] Piping data into ephemeral Docker process as STDIN and receiving STDOUT back out.

```bash
< fixture/outage_20200101.csv.gz gunzip | head -5 | docker run -i --rm psa-star:0.1 bin/csv-to-psa-format config/outage_config.edn
nk,vtime,attrs
312,1999-05-17 00:00:00,...
...
```

# Tutorial

High level goal: Starting with a CSV file, we want to put it into a PSA table, which is a document store for a data model entity. We want to turn this:

```csv
id,a,b,c
1,d,e,f
2,g,h,i
3,j,k,l
```

Into this:

```csv
nk,vtime,attrs
1,2021-08-01 13:52:47,"{\"a\":\"d\",\"b\":\"e\",\"c\":\"f\"}"
2,2021-08-01 13:52:47,"{\"a\":\"g\",\"b\":\"h\",\"c\":\"i\"}"
3,2021-08-01 13:52:47,"{\"a\":\"j\",\"b\":\"k\",\"c\":\"l\"}"
```

Let's take a bottom's up approach as an example.


We have an input CSV from our customer. Let's just work with 4 records for simplicity.


```bash
< fixture/outage_20200101.csv.gz gunzip | head -5
```

```
"OUTAGE_ID","OUTAGE_STATUS","OUTAGE_TYPE","UNIT_ID","START_DATE","END_DATE","LAST_START_DATE","LAST_END_DATE","LAST_DATE","PREV_DATE","OUTAGE_PRECISION","ENTRY_DATE","RELEASE_DATE","UNIT_NAME","UNIT_STATUS","PLANT_ID","UNIT_TYPE","PLANT_NAME","STATE_NAME","INDUSTRY_DESC","PHYS_COUNTRY","FEED_DESIGN_CAPACITY","FEED_ACTUAL_CAPACITY","FEED_UNIT_MEASURE","FEED_HS_PRODUCT","OUTPUT_DESIGN_CAPACITY","OUTPUT_ACTUAL_CAPACITY","OUTPUT_UNIT_MEASURE","OUTPUT_HS_PRODUCT","COMMENT_MV_ORDER","DERATE","UNIT_CAPACITY_SNAPSHOT","COMMENTS","VERSION","VERSION_DATE","UNIT_CAPACITY","CAPACITY_UOM","CAPACITY_OFFLINE","OUTAGE_DURATION","RN","snap_date"
312,"Past","Planned",1009045,1998-01-31 11:00:00,1998-03-16 11:00:00,NA,NA,1999-05-17 00:00:00,1997-11-20 23:00:00,"DAY",1997-11-20 17:00:00,1999-05-17 00:00:00,"Boiler Plant","Operational",1007860,"Boiler House","Los Angeles Refinery - Wilmington","CA","Petroleum Refining (HPI)","US",NA,NA,NA,NA,50,55,"MWh","009981",NA,100,50,NA,"3 of 3",2012-05-01 19:00:00,50,"MWh",0,45,740,"20200101"
485,"Past","Planned",1002106,1997-12-31 11:00:00,1998-01-13 11:00:00,NA,NA,1998-12-21 23:00:00,1997-11-20 23:00:00,"MONTH",1997-11-20 17:00:00,1998-12-21 23:00:00,"GEN A CT/HRSG","Operational",1014570,"Simple Cycle Combustion Turbine","Rodeo Refinery","CA","Petroleum Refining (HPI)","US",NA,NA,NA,NA,17.19,15.6,"W","009981",NA,100,17,NA,"1 of 1",1997-11-20 17:00:00,17.19,"W",17.19,14,1165,"20200101"
604,"Past","Planned",1002144,1997-02-28 11:00:00,1997-03-06 11:00:00,NA,NA,1997-11-20 23:00:00,NA,"MONTH",1997-11-20 17:00:00,1997-11-20 23:00:00,"Cogen CC1-01 GT/HRSG","Operational",1000293,"Combined Cycle Combustion Turbine","Richmond Refinery","CA","Petroleum Refining (HPI)","US",NA,NA,NA,NA,62.6,51.2,"W","009981",NA,100,63,NA,"1 of 1",1997-11-20 17:00:00,62.6,"W",62.6,7,1446,"20200101"
792,"Past","Planned",1000196,1997-09-30 12:00:00,1997-09-30 12:00:00,1997-10-01 00:00:00,1997-09-30 00:00:00,2012-10-24 12:49:05,1997-11-20 23:00:00,"MONTH",1997-11-20 17:00:00,2012-10-24 12:49:09,"FCCU","Closed",1009399,"FCCU (Fluid Catalytic Cracker)","Blue Island Refinery","IL","Petroleum Refining (HPI)","US",NA,38000,"BBL/d","001645",NA,NA,NA,NA,NA,100,38000,NA,"2 of 2",2012-10-24 07:49:05,38000,"BBL/d",38000,1,1890,"20200101"
```


Reading CSV in the raw is hard for humans, so from here on out we'll use `xsv` to pretty print CSV output in a human-readable way. You can install xsv with `brew install xsv` or `sudo apt install xsv`.


```bash
< fixture/outage_20200101.csv.gz gunzip | head -5 | xsv table
```

```
OUTAGE_ID  OUTAGE_STATUS  OUTAGE_TYPE  UNIT_ID  START_DATE           END_DATE             LAST_START_DATE      LAST_END_DATE        LAST_DATE            PREV_DATE            OUTAGE_PRECISION  ENTRY_DATE           RELEASE_DATE         UNIT_NAME             UNIT_STATUS  PLANT_ID  UNIT_TYPE                          PLANT_NAME                         STATE_NAME  INDUSTRY_DESC             PHYS_COUNTRY  FEED_DESIGN_CAPACITY  FEED_ACTUAL_CAPACITY  FEED_UNIT_MEASURE  FEED_HS_PRODUCT  OUTPUT_DESIGN_CAPACITY  OUTPUT_ACTUAL_CAPACITY  OUTPUT_UNIT_MEASURE  OUTPUT_HS_PRODUCT  COMMENT_MV_ORDER  DERATE  UNIT_CAPACITY_SNAPSHOT  COMMENTS  VERSION  VERSION_DATE         UNIT_CAPACITY  CAPACITY_UOM  CAPACITY_OFFLINE  OUTAGE_DURATION  RN    snap_date
312        Past           Planned      1009045  1998-01-31 11:00:00  1998-03-16 11:00:00  NA                   NA                   1999-05-17 00:00:00  1997-11-20 23:00:00  DAY               1997-11-20 17:00:00  1999-05-17 00:00:00  Boiler Plant          Operational  1007860   Boiler House                       Los Angeles Refinery - Wilmington  CA          Petroleum Refining (HPI)  US            NA                    NA                    NA                 NA               50                      55                      MWh                  009981             NA                100     50                      NA        3 of 3   2012-05-01 19:00:00  50             MWh           0                 45               740   20200101
485        Past           Planned      1002106  1997-12-31 11:00:00  1998-01-13 11:00:00  NA                   NA                   1998-12-21 23:00:00  1997-11-20 23:00:00  MONTH             1997-11-20 17:00:00  1998-12-21 23:00:00  GEN A CT/HRSG         Operational  1014570   Simple Cycle Combustion Turbine    Rodeo Refinery                     CA          Petroleum Refining (HPI)  US            NA                    NA                    NA                 NA               17.19                   15.6                    W                    009981             NA                100     17                      NA        1 of 1   1997-11-20 17:00:00  17.19          W             17.19             14               1165  20200101
604        Past           Planned      1002144  1997-02-28 11:00:00  1997-03-06 11:00:00  NA                   NA                   1997-11-20 23:00:00  NA                   MONTH             1997-11-20 17:00:00  1997-11-20 23:00:00  Cogen CC1-01 GT/HRSG  Operational  1000293   Combined Cycle Combustion Turbine  Richmond Refinery                  CA          Petroleum Refining (HPI)  US            NA                    NA                    NA                 NA               62.6                    51.2                    W                    009981             NA                100     63                      NA        1 of 1   1997-11-20 17:00:00  62.6           W             62.6              7                1446  20200101
792        Past           Planned      1000196  1997-09-30 12:00:00  1997-09-30 12:00:00  1997-10-01 00:00:00  1997-09-30 00:00:00  2012-10-24 12:49:05  1997-11-20 23:00:00  MONTH             1997-11-20 17:00:00  2012-10-24 12:49:09  FCCU                  Closed       1009399   FCCU (Fluid Catalytic Cracker)     Blue Island Refinery               IL          Petroleum Refining (HPI)  US            NA                    38000                 BBL/d              001645           NA                      NA                      NA                   NA                 NA                100     38000                   NA        2 of 2   2012-10-24 07:49:05  38000          BBL/d         38000             1                1890  20200101
```


Let's clean it up and transform it to PSA-formatted CSV. This will include conforming the headers by lower snakecasing them, then converting the CSV record into a JSON field called `attrs` in our PSA. The natural key (nk) will be the `outage_id`. We will copy the CSV's `release_date` field as our valid time (`vtime`) value. This as well type definitions for _expected columns_ are defined in an entity-specific configuration file which we pass to `bin/csv-to-psa-format` as an argument. If our input CSV is missing any of the expected columns, or if any of the expected columns' values do not coerce to their expected types, an error will be thrown.

```bash
cat config/outage_config.edn
```

```clojure
{
 :nk_cols [
           :outage_id
           ]
 :copy_cols {
             :release_date :vtime
             }
 :expect_cols  {
                :outage_id        :int
                :vtime            :db_timestamp_string
                :outage_status    :string
                :outage_type      :string
                :unit_id          :int
                :plant_id         :int
                :start_date       :db_date_string ; chops off time.
                :end_date         :db_date_string ; chops off time.
                :release_date     :db_timestamp_string
                :capacity_offline :float
                :capacity_uom     :string
                :snap_date        :db_date_string
                }
 }
```

```bash
< fixture/outage_20200101.csv.gz gunzip | head -5 | bin/csv-to-psa-format config/outage_config.edn | xsv table
```

```
nk   vtime                attrs
312  1999-05-17 00:00:00  "{""outage_id"":312,""outage_status"":""Past"",""outage_type"":""Planned"",""unit_id"":1009045,""start_date"":""1998-01-31"",""end_date"":""1998-03-16"",""last_start_date"":""NA"",""last_end_date"":""NA"",""last_date"":""1999-05-17 00:00:00"",""prev_date"":""1997-11-20 23:00:00"",""outage_precision"":""DAY"",""entry_date"":""1997-11-20 17:00:00"",""release_date"":""1999-05-17 00:00:00"",""unit_name"":""Boiler Plant"",""unit_status"":""Operational"",""plant_id"":1007860,""unit_type"":""Boiler House"",""plant_name"":""Los Angeles Refinery - Wilmington"",""state_name"":""CA"",""industry_desc"":""Petroleum Refining (HPI)"",""phys_country"":""US"",""feed_design_capacity"":""NA"",""feed_actual_capacity"":""NA"",""feed_unit_measure"":""NA"",""feed_hs_product"":""NA"",""output_design_capacity"":""50"",""output_actual_capacity"":""55"",""output_unit_measure"":""MWh"",""output_hs_product"":""009981"",""comment_mv_order"":""NA"",""derate"":""100"",""unit_capacity_snapshot"":""50"",""comments"":""NA"",""version"":""3 of 3"",""version_date"":""2012-05-01 19:00:00"",""unit_capacity"":""50"",""capacity_uom"":""MWh"",""capacity_offline"":0.0,""outage_duration"":""45"",""rn"":""740"",""snap_date"":""2020-01-01"",""vtime"":""1999-05-17 00:00:00""}"
485  1998-12-21 23:00:00  "{""outage_id"":485,""outage_status"":""Past"",""outage_type"":""Planned"",""unit_id"":1002106,""start_date"":""1997-12-31"",""end_date"":""1998-01-13"",""last_start_date"":""NA"",""last_end_date"":""NA"",""last_date"":""1998-12-21 23:00:00"",""prev_date"":""1997-11-20 23:00:00"",""outage_precision"":""MONTH"",""entry_date"":""1997-11-20 17:00:00"",""release_date"":""1998-12-21 23:00:00"",""unit_name"":""GEN A CT/HRSG"",""unit_status"":""Operational"",""plant_id"":1014570,""unit_type"":""Simple Cycle Combustion Turbine"",""plant_name"":""Rodeo Refinery"",""state_name"":""CA"",""industry_desc"":""Petroleum Refining (HPI)"",""phys_country"":""US"",""feed_design_capacity"":""NA"",""feed_actual_capacity"":""NA"",""feed_unit_measure"":""NA"",""feed_hs_product"":""NA"",""output_design_capacity"":""17.19"",""output_actual_capacity"":""15.6"",""output_unit_measure"":""W"",""output_hs_product"":""009981"",""comment_mv_order"":""NA"",""derate"":""100"",""unit_capacity_snapshot"":""17"",""comments"":""NA"",""version"":""1 of 1"",""version_date"":""1997-11-20 17:00:00"",""unit_capacity"":""17.19"",""capacity_uom"":""W"",""capacity_offline"":17.19,""outage_duration"":""14"",""rn"":""1165"",""snap_date"":""2020-01-01"",""vtime"":""1998-12-21 23:00:00""}"
604  1997-11-20 23:00:00  "{""outage_id"":604,""outage_status"":""Past"",""outage_type"":""Planned"",""unit_id"":1002144,""start_date"":""1997-02-28"",""end_date"":""1997-03-06"",""last_start_date"":""NA"",""last_end_date"":""NA"",""last_date"":""1997-11-20 23:00:00"",""prev_date"":""NA"",""outage_precision"":""MONTH"",""entry_date"":""1997-11-20 17:00:00"",""release_date"":""1997-11-20 23:00:00"",""unit_name"":""Cogen CC1-01 GT/HRSG"",""unit_status"":""Operational"",""plant_id"":1000293,""unit_type"":""Combined Cycle Combustion Turbine"",""plant_name"":""Richmond Refinery"",""state_name"":""CA"",""industry_desc"":""Petroleum Refining (HPI)"",""phys_country"":""US"",""feed_design_capacity"":""NA"",""feed_actual_capacity"":""NA"",""feed_unit_measure"":""NA"",""feed_hs_product"":""NA"",""output_design_capacity"":""62.6"",""output_actual_capacity"":""51.2"",""output_unit_measure"":""W"",""output_hs_product"":""009981"",""comment_mv_order"":""NA"",""derate"":""100"",""unit_capacity_snapshot"":""63"",""comments"":""NA"",""version"":""1 of 1"",""version_date"":""1997-11-20 17:00:00"",""unit_capacity"":""62.6"",""capacity_uom"":""W"",""capacity_offline"":62.6,""outage_duration"":""7"",""rn"":""1446"",""snap_date"":""2020-01-01"",""vtime"":""1997-11-20 23:00:00""}"
792  2012-10-24 12:49:09  "{""outage_id"":792,""outage_status"":""Past"",""outage_type"":""Planned"",""unit_id"":1000196,""start_date"":""1997-09-30"",""end_date"":""1997-09-30"",""last_start_date"":""1997-10-01 00:00:00"",""last_end_date"":""1997-09-30 00:00:00"",""last_date"":""2012-10-24 12:49:05"",""prev_date"":""1997-11-20 23:00:00"",""outage_precision"":""MONTH"",""entry_date"":""1997-11-20 17:00:00"",""release_date"":""2012-10-24 12:49:09"",""unit_name"":""FCCU"",""unit_status"":""Closed"",""plant_id"":1009399,""unit_type"":""FCCU (Fluid Catalytic Cracker)"",""plant_name"":""Blue Island Refinery"",""state_name"":""IL"",""industry_desc"":""Petroleum Refining (HPI)"",""phys_country"":""US"",""feed_design_capacity"":""NA"",""feed_actual_capacity"":""38000"",""feed_unit_measure"":""BBL/d"",""feed_hs_product"":""001645"",""output_design_capacity"":""NA"",""output_actual_capacity"":""NA"",""output_unit_measure"":""NA"",""output_hs_product"":""NA"",""comment_mv_order"":""NA"",""derate"":""100"",""unit_capacity_snapshot"":""38000"",""comments"":""NA"",""version"":""2 of 2"",""version_date"":""2012-10-24 07:49:05"",""unit_capacity"":""38000"",""capacity_uom"":""BBL/d"",""capacity_offline"":38000.0,""outage_duration"":""1"",""rn"":""1890"",""snap_date"":""2020-01-01"",""vtime"":""2012-10-24 12:49:09""}"
```


That looks good. Now, we need to put it into our database. We have a PostgreSQL instance and a wrapper psql script in `bin/psql` for that. Let's stand up a table for it in our database called `psa_outage`. `bin/psa-generate-ddl-boilerlate` take a table name and generates the necessary DDL boilerplate to define the table and all the triggers that maintain valid time.


```bash
bin/psa-generate-ddl-boilerlate psa_outage
```

```sql
BEGIN;

-- Define table.
create table if not exists psa_outage (
    nk           text         not null
  , attrs_digest uuid         generated always as (md5(attrs::text)::uuid) stored
  , vtime        timestamp(0) default now()::timestamp(0)
  , xtime        timestamp(0) default '9999-12-31 23:59:59'::timestamp(0) check (xtime>vtime)
  , attrs        json         not null default '{}'::json
  , primary key  (nk, vtime)
  -- , unique      (nk, xtime)
);

-- Define the TRIGGER FUNCTION.
create or replace function psa_outage__recalc_xtimes()
  returns trigger
  language plpgsql
  as
$$
begin
  delete from psa_outage
  where nk = new.nk
  and vtime = new.vtime
  and attrs_digest = (
    select attrs_digest
    from psa_outage
    where nk = new.nk
    and vtime = (select max(vtime) from psa_outage sub where nk = new.nk and vtime < new.vtime)
  )
  ;
  update psa_outage
  set xtime = coalesce((select min(vtime) from psa_outage sub where nk = psa_outage.nk and vtime > psa_outage.vtime), '9999-12-31 23:59:59'::timestamp(0))
  where nk = new.nk
  and xtime <> coalesce((select min(vtime) from psa_outage sub where nk = psa_outage.nk and vtime > psa_outage.vtime), '9999-12-31 23:59:59'::timestamp(0))
  ;
  return new;
end;
$$;

-- Define the INSERT & DELETE TRIGGERS.
drop trigger if exists trigger__psa_outage__insert__recalc_xtimes on psa_outage;
drop trigger if exists trigger__psa_outage__delete__recalc_xtimes on psa_outage;
create trigger trigger__psa_outage__insert__recalc_xtimes after insert on psa_outage for each row execute procedure psa_outage__recalc_xtimes() ;
create trigger trigger__psa_outage__delete__recalc_xtimes after delete on psa_outage for each row execute procedure psa_outage__recalc_xtimes() ;

COMMIT;
```

To execute the DDL, we pipe that SQL code to the database.

```
bin/psa-generate-ddl-boilerlate psa_outage | bin/psql
```

```
BEGIN
CREATE TABLE
CREATE FUNCTION
NOTICE:  trigger "trigger__psa_outage__insert__recalc_xtimes" for relation "psa_outage" does not exist, skipping
DROP TRIGGER
NOTICE:  trigger "trigger__psa_outage__delete__recalc_xtimes" for relation "psa_outage" does not exist, skipping
DROP TRIGGER
CREATE TRIGGER
CREATE TRIGGER
COMMIT
```

Now the PSA table is stood up. Let's convert our PSA-formatted CSV into SQL upsert statements and send it in to the table via an upsert. We will use `bin/csv-to-db-insert` for that, passing it a table name and a comma-delimited list of the primary key.


```
< fixture/outage_20200101.csv.gz gunzip | head -5 | bin/csv-to-psa-format config/outage_config.edn | bin/csv-to-db-insert psa_outage -k nk,vtime
```

```
INSERT INTO psa_outage (nk,vtime,attrs) VALUES
('312','1999-05-17 00:00:00','{"outage_id":312,"outage_status":"Past","outage_type":"Planned","unit_id":1009045,"start_date":"1998-01-31","end_date":"1998-03-16","last_start_date":"NA","last_end_date":"NA","last_date":"1999-05-17 00:00:00","prev_date":"1997-11-20 23:00:00","outage_precision":"DAY","entry_date":"1997-11-20 17:00:00","release_date":"1999-05-17 00:00:00","unit_name":"Boiler Plant","unit_status":"Operational","plant_id":1007860,"unit_type":"Boiler House","plant_name":"Los Angeles Refinery - Wilmington","state_name":"CA","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"NA","feed_unit_measure":"NA","feed_hs_product":"NA","output_design_capacity":"50","output_actual_capacity":"55","output_unit_measure":"MWh","output_hs_product":"009981","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"50","comments":"NA","version":"3 of 3","version_date":"2012-05-01 19:00:00","unit_capacity":"50","capacity_uom":"MWh","capacity_offline":0.0,"outage_duration":"45","rn":"740","snap_date":"2020-01-01","vtime":"1999-05-17 00:00:00"}')
,('485','1998-12-21 23:00:00','{"outage_id":485,"outage_status":"Past","outage_type":"Planned","unit_id":1002106,"start_date":"1997-12-31","end_date":"1998-01-13","last_start_date":"NA","last_end_date":"NA","last_date":"1998-12-21 23:00:00","prev_date":"1997-11-20 23:00:00","outage_precision":"MONTH","entry_date":"1997-11-20 17:00:00","release_date":"1998-12-21 23:00:00","unit_name":"GEN A CT/HRSG","unit_status":"Operational","plant_id":1014570,"unit_type":"Simple Cycle Combustion Turbine","plant_name":"Rodeo Refinery","state_name":"CA","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"NA","feed_unit_measure":"NA","feed_hs_product":"NA","output_design_capacity":"17.19","output_actual_capacity":"15.6","output_unit_measure":"W","output_hs_product":"009981","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"17","comments":"NA","version":"1 of 1","version_date":"1997-11-20 17:00:00","unit_capacity":"17.19","capacity_uom":"W","capacity_offline":17.19,"outage_duration":"14","rn":"1165","snap_date":"2020-01-01","vtime":"1998-12-21 23:00:00"}')
ON CONFLICT (nk,vtime) DO UPDATE SET
nk = EXCLUDED.nk
,vtime = EXCLUDED.vtime
,attrs = EXCLUDED.attrs
;

INSERT INTO psa_outage (nk,vtime,attrs) VALUES
('604','1997-11-20 23:00:00','{"outage_id":604,"outage_status":"Past","outage_type":"Planned","unit_id":1002144,"start_date":"1997-02-28","end_date":"1997-03-06","last_start_date":"NA","last_end_date":"NA","last_date":"1997-11-20 23:00:00","prev_date":"NA","outage_precision":"MONTH","entry_date":"1997-11-20 17:00:00","release_date":"1997-11-20 23:00:00","unit_name":"Cogen CC1-01 GT/HRSG","unit_status":"Operational","plant_id":1000293,"unit_type":"Combined Cycle Combustion Turbine","plant_name":"Richmond Refinery","state_name":"CA","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"NA","feed_unit_measure":"NA","feed_hs_product":"NA","output_design_capacity":"62.6","output_actual_capacity":"51.2","output_unit_measure":"W","output_hs_product":"009981","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"63","comments":"NA","version":"1 of 1","version_date":"1997-11-20 17:00:00","unit_capacity":"62.6","capacity_uom":"W","capacity_offline":62.6,"outage_duration":"7","rn":"1446","snap_date":"2020-01-01","vtime":"1997-11-20 23:00:00"}')
,('792','2012-10-24 12:49:09','{"outage_id":792,"outage_status":"Past","outage_type":"Planned","unit_id":1000196,"start_date":"1997-09-30","end_date":"1997-09-30","last_start_date":"1997-10-01 00:00:00","last_end_date":"1997-09-30 00:00:00","last_date":"2012-10-24 12:49:05","prev_date":"1997-11-20 23:00:00","outage_precision":"MONTH","entry_date":"1997-11-20 17:00:00","release_date":"2012-10-24 12:49:09","unit_name":"FCCU","unit_status":"Closed","plant_id":1009399,"unit_type":"FCCU (Fluid Catalytic Cracker)","plant_name":"Blue Island Refinery","state_name":"IL","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"38000","feed_unit_measure":"BBL/d","feed_hs_product":"001645","output_design_capacity":"NA","output_actual_capacity":"NA","output_unit_measure":"NA","output_hs_product":"NA","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"38000","comments":"NA","version":"2 of 2","version_date":"2012-10-24 07:49:05","unit_capacity":"38000","capacity_uom":"BBL/d","capacity_offline":38000.0,"outage_duration":"1","rn":"1890","snap_date":"2020-01-01","vtime":"2012-10-24 12:49:09"}')
ON CONFLICT (nk,vtime) DO UPDATE SET
nk = EXCLUDED.nk
,vtime = EXCLUDED.vtime
,attrs = EXCLUDED.attrs
;

```

```bash
< fixture/outage_20200101.csv.gz gunzip | head -5 | bin/csv-to-psa-format config/outage_config.edn | bin/csv-to-db-insert psa_outage -k nk,vtime | bin/psql
```

```bash
INSERT 0 2
INSERT 0 2
```

Great. Lets check what we have by running a `SELECT *` against our table, formatting it vertically for extra readibility.

```bash
bin/psql -c "\x" -c "select * from psa_outage"
```

```
Expanded display is on.
-[ RECORD 1 ]+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
nk           | 312
attrs_digest | e46f0325-055b-f1e0-40b5-75525590de05
vtime        | 1999-05-17 00:00:00
xtime        | 9999-12-31 23:59:59
attrs        | {"outage_id":312,"outage_status":"Past","outage_type":"Planned","unit_id":1009045,"start_date":"1998-01-31","end_date":"1998-03-16","last_start_date":"NA","last_end_date":"NA","last_date":"1999-05-17 00:00:00","prev_date":"1997-11-20 23:00:00","outage_precision":"DAY","entry_date":"1997-11-20 17:00:00","release_date":"1999-05-17 00:00:00","unit_name":"Boiler Plant","unit_status":"Operational","plant_id":1007860,"unit_type":"Boiler House","plant_name":"Los Angeles Refinery - Wilmington","state_name":"CA","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"NA","feed_unit_measure":"NA","feed_hs_product":"NA","output_design_capacity":"50","output_actual_capacity":"55","output_unit_measure":"MWh","output_hs_product":"009981","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"50","comments":"NA","version":"3 of 3","version_date":"2012-05-01 19:00:00","unit_capacity":"50","capacity_uom":"MWh","capacity_offline":0.0,"outage_duration":"45","rn":"740","snap_date":"2020-01-01","vtime":"1999-05-17 00:00:00"}
-[ RECORD 2 ]+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
nk           | 485
attrs_digest | 7b32486d-b4fc-d616-4713-c917654af979
vtime        | 1998-12-21 23:00:00
xtime        | 9999-12-31 23:59:59
attrs        | {"outage_id":485,"outage_status":"Past","outage_type":"Planned","unit_id":1002106,"start_date":"1997-12-31","end_date":"1998-01-13","last_start_date":"NA","last_end_date":"NA","last_date":"1998-12-21 23:00:00","prev_date":"1997-11-20 23:00:00","outage_precision":"MONTH","entry_date":"1997-11-20 17:00:00","release_date":"1998-12-21 23:00:00","unit_name":"GEN A CT/HRSG","unit_status":"Operational","plant_id":1014570,"unit_type":"Simple Cycle Combustion Turbine","plant_name":"Rodeo Refinery","state_name":"CA","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"NA","feed_unit_measure":"NA","feed_hs_product":"NA","output_design_capacity":"17.19","output_actual_capacity":"15.6","output_unit_measure":"W","output_hs_product":"009981","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"17","comments":"NA","version":"1 of 1","version_date":"1997-11-20 17:00:00","unit_capacity":"17.19","capacity_uom":"W","capacity_offline":17.19,"outage_duration":"14","rn":"1165","snap_date":"2020-01-01","vtime":"1998-12-21 23:00:00"}
-[ RECORD 3 ]+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
nk           | 604
attrs_digest | 237bfee5-e0ef-adcc-4964-9a77babb869b
vtime        | 1997-11-20 23:00:00
xtime        | 9999-12-31 23:59:59
attrs        | {"outage_id":604,"outage_status":"Past","outage_type":"Planned","unit_id":1002144,"start_date":"1997-02-28","end_date":"1997-03-06","last_start_date":"NA","last_end_date":"NA","last_date":"1997-11-20 23:00:00","prev_date":"NA","outage_precision":"MONTH","entry_date":"1997-11-20 17:00:00","release_date":"1997-11-20 23:00:00","unit_name":"Cogen CC1-01 GT/HRSG","unit_status":"Operational","plant_id":1000293,"unit_type":"Combined Cycle Combustion Turbine","plant_name":"Richmond Refinery","state_name":"CA","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"NA","feed_unit_measure":"NA","feed_hs_product":"NA","output_design_capacity":"62.6","output_actual_capacity":"51.2","output_unit_measure":"W","output_hs_product":"009981","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"63","comments":"NA","version":"1 of 1","version_date":"1997-11-20 17:00:00","unit_capacity":"62.6","capacity_uom":"W","capacity_offline":62.6,"outage_duration":"7","rn":"1446","snap_date":"2020-01-01","vtime":"1997-11-20 23:00:00"}
-[ RECORD 4 ]+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
nk           | 792
attrs_digest | 4b2c0ca7-bb24-7f67-2ec8-f14b2dd46e7a
vtime        | 2012-10-24 12:49:09
xtime        | 9999-12-31 23:59:59
attrs        | {"outage_id":792,"outage_status":"Past","outage_type":"Planned","unit_id":1000196,"start_date":"1997-09-30","end_date":"1997-09-30","last_start_date":"1997-10-01 00:00:00","last_end_date":"1997-09-30 00:00:00","last_date":"2012-10-24 12:49:05","prev_date":"1997-11-20 23:00:00","outage_precision":"MONTH","entry_date":"1997-11-20 17:00:00","release_date":"2012-10-24 12:49:09","unit_name":"FCCU","unit_status":"Closed","plant_id":1009399,"unit_type":"FCCU (Fluid Catalytic Cracker)","plant_name":"Blue Island Refinery","state_name":"IL","industry_desc":"Petroleum Refining (HPI)","phys_country":"US","feed_design_capacity":"NA","feed_actual_capacity":"38000","feed_unit_measure":"BBL/d","feed_hs_product":"001645","output_design_capacity":"NA","output_actual_capacity":"NA","output_unit_measure":"NA","output_hs_product":"NA","comment_mv_order":"NA","derate":"100","unit_capacity_snapshot":"38000","comments":"NA","version":"2 of 2","version_date":"2012-10-24 07:49:05","unit_capacity":"38000","capacity_uom":"BBL/d","capacity_offline":38000.0,"outage_duration":"1","rn":"1890","snap_date":"2020-01-01","vtime":"2012-10-24 12:49:09"}
```

And we're done!
